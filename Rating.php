<?php

include 'Website.php';

class Rating {

    public function getVisitorsOfWebsite($websites, $domain){

        foreach ($websites as $website){
            if($website->domain == $domain){
                return 'Кількість відвідувачів вебсайту '. $website->domain.' : '.$website->visitors."\n";
            }
        }
    }
}