<?php

include 'Rating.php';

use  \PHPUnit\Framework\TestCase;

class WebsiteTest extends TestCase {

    /**
     * @dataProvider websitesDataProvider
     * @param $result
     * @param $expected
     */

    public function testGetVisitorsOfWebsite($websites, $expected) {
        $result = (new Rating)->getVisitorsOfWebsite($websites,'https://tpza.kpi.ua/');
        $this->assertEquals($expected, $result);
    }

    public function websitesDataProvider() {
        return array(
            array(
                array(
                    new Website('https://tpza.kpi.ua/', 10000),
                    new Website ('https://gitlab.com/', 100000),
                    new Website('https://kpi.ua/', 25000),
                ), 10000),

            array(
                array(
                    new Website('https://tpza.kpi.ua/', 10000),
                    new Website ('https://gitlab.com/', 100000),
                    new Website('https://kpi.ua/', 25000),
                ), 999999),

        );
    }
}