<?php
    include 'Rating.php';

    $rating = new Rating();
    $websites = array(
        new Website('https://tpza.kpi.ua/', 10000),
        new Website ('https://gitlab.com/', 100000),
        new Website('https://kpi.ua/', 25000),
    );

    echo $rating->getVisitorsOfWebsite($websites,'https://kpi.ua/');
    echo $rating->getVisitorsOfWebsite($websites,'https://tpza.kpi.ua/');


