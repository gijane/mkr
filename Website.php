<?php


class Website {

    public $domain;
    public $visitors;

    /**
     * Website constructor.
     * @param $domain
     * @param $visitors
     */
    public function __construct($domain, $visitors) {
        $this->domain = $domain;
        $this->visitors = $visitors;
    }

    /**
     * @return mixed
     */
    public function getDomain() {
        return $this->domain;
    }

    /**
     * @param mixed $domain
     */
    public function setDomain($domain) {
        $this->domain = $domain;
    }

    /**
     * @return mixed
     */
    public function getVisitors() {
        return $this->visitors;
    }

    /**
     * @param mixed $visitors
     */
    public function setVisitors($visitors) {
        $this->visitors = $visitors;
    }
}